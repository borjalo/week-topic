import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IonNav, ModalController } from '@ionic/angular';
import { AuthService } from '../../services/auth.service';
import { SignupPage } from '../signup/signup.page';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  submitError: string;

  validationMessages = {
    email: [
      { type: 'required', message: 'Campo obligatorio.' },
      { type: 'pattern', message: 'Introduce un email válido.' },
    ],
    password: [
      { type: 'required', message: 'Campo obligatorio.' },
      {
        type: 'minlength',
        message: 'La contraseña debe contener al menos 6 caracteres.',
      },
    ],
  };
  showPassword = false;

  constructor(
    public modalCtrl: ModalController,
    private authService: AuthService,
    private nav: IonNav
  ) {
    this.loginForm = new FormGroup({
      email: new FormControl(
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'),
        ])
      ),
      password: new FormControl(
        '',
        Validators.compose([Validators.minLength(6), Validators.required])
      ),
    });
  }

  ngOnInit() {}

  toggleShow() {
    this.showPassword = !this.showPassword;
    const passwordInput: any = document.getElementById('passwordInput');

    passwordInput.type = this.showPassword ? 'text' : 'password';
  }

  async close() {
    await this.modalCtrl.dismiss();
  }

  async login(credentials) {
    try {
      await this.authService.emailLogin(credentials);
      await this.close();
    } catch (e) {
      this.submitError = e;
    }
  }

  async goForward() {
    await this.nav.push(SignupPage);
  }
}
