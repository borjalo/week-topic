import { Component } from '@angular/core';
import {
  IonRouterOutlet,
  ModalController,
  PopoverController,
} from '@ionic/angular';
import { LoginPage } from '../login/login.page';
import { AuthService } from '../../services/auth.service';
import { FirestoreService } from '../../services/firestore.service';
import { ModalBaseComponent } from '../../components/modal-base/modal-base.component';
import { PopoverComponent } from '../../components/popover/popover.component';
import { HistoryPage } from '../history/history.page';
import { Message } from '../../interfaces/message';
import { Topic } from '../../interfaces/topic';
import { onSnapshot } from '@angular/fire/firestore';
import { DocumentData } from '@angular/fire/compat/firestore';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public message = '';
  public messages: Message[];
  public actualTopic: Topic;
  public uid = '';
  private userName = '';

  constructor(
    public modalController: ModalController,
    public authService: AuthService,
    public dbService: FirestoreService,
    private routerOutlet: IonRouterOutlet,
    private popoverController: PopoverController
  ) {
    this.authService.firebaseUser$.subscribe((user) => {
      if (user) {
        this.userName = user.displayName;
        this.uid = user.uid;
        this.messages = this.dbService.getMessages();
      }
    });

    this.listenToActualTopic();
  }

  listenToActualTopic() {
    const topicQuery = this.dbService.getActualTopic();

    onSnapshot(topicQuery, (querySnapshot) => {
      querySnapshot.forEach((document: DocumentData) => {
        this.actualTopic = document.data();
        this.actualTopic.id = document.id;
      });
    });
  }

  async openHistoryModal() {
    const modal = await this.modalController.create({
      component: HistoryPage,
    });

    return await modal.present();
  }

  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: PopoverComponent,
      event: ev,
      translucent: true,
    });
    return await popover.present();
  }

  async openLoginModal() {
    const loginModal = await this.modalController.create({
      presentingElement: this.routerOutlet.nativeEl,
      component: ModalBaseComponent,
      componentProps: {
        rootPage: LoginPage,
      },
    });

    loginModal.onDidDismiss().then((data) => {
      this.userName = data.data.userName;
    });

    return await loginModal.present();
  }

  async upvoteTopic() {
    if (this.uid) {
      await this.dbService.upvoteTopic(this.actualTopic.id, this.uid);
    } else {
      await this.openLoginModal();
    }
  }

  async removeUpvoteFromTopic() {
    if (this.uid) {
      await this.dbService.removeUpvoteFromTopic(this.actualTopic.id, this.uid);
    }
  }

  async downvoteTopic() {
    if (this.uid) {
      await this.dbService.downvoteTopic(this.actualTopic.id, this.uid);
    } else {
      await this.openLoginModal();
    }
  }

  async removeDownvoteFromTopic() {
    if (this.uid) {
      await this.dbService.removeDownvoteFromTopic(
        this.actualTopic.id,
        this.uid
      );
    }
  }

  async addMessage() {
    await this.dbService.addChatMessage(
      this.message,
      this.userName,
      this.uid,
      this.actualTopic.id
    );
    setTimeout(() => {
      const list = document.getElementById('contentList');
      list.scroll(0, 1000);
    }, 500);
    this.message = '';
  }

  async likeMessage(messageId: string) {
    await this.dbService.likeMessage(messageId, this.uid);
  }

  async unlikeMessage(messageId: string) {
    await this.dbService.unlikeMessage(messageId, this.uid);
  }
}
