import { Component, OnInit } from '@angular/core';
import { Topic } from '../../interfaces/topic';
import { ModalController } from '@ionic/angular';
import { FirestoreService } from '../../services/firestore.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {
  public topics: Topic[];

  constructor(
    private modalCtrl: ModalController,
    private dbService: FirestoreService
  ) { }

  async ngOnInit() {
    this.topics = await this.dbService.getHistoryTopics();
  }

  async close() {
    await this.modalCtrl.dismiss();
  }
}
