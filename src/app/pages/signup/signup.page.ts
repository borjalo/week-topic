import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  registerForm: FormGroup;
  submitError: string;

  validationMessages = {
    userName: [
      { type: 'minlength', message: 'El nombre de usuario debe contener al menos 2 caracteres.' }
    ],
    email: [
      { type: 'required', message: 'Campo obligatorio.' },
      { type: 'pattern', message: 'Introduce un email válido.' }
    ],
    password: [
      { type: 'required', message: 'Campo obligatorio.' },
      { type: 'minlength', message: 'La contraseña debe contener al menos 6 caracteres.' }
    ]
  };
  public showPassword = false;

  constructor(private authService: AuthService, private modalController: ModalController) {
    this.registerForm = new FormGroup({
      userName: new FormControl('', Validators.compose([
        Validators.minLength(2),
        Validators.required
      ])),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(6),
        Validators.required
      ]))
    });
  }

  ngOnInit() {
  }

  toggleShow() {
    this.showPassword = !this.showPassword;
    const passwordInput: any = document.getElementById('passwordInput2');

    passwordInput.type = this.showPassword ? 'text' : 'password';
  }

  async signup(credentials) {
    try {
      await this.authService.emailSignUp(credentials);
      await this.modalController.dismiss({ userName: credentials.userName });
    } catch (e) {
      this.submitError = this.authService.handleAuthError(e);
    }
  }
}
