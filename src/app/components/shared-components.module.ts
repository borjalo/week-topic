import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalBaseComponent } from './modal-base/modal-base.component';
import { IonicModule } from '@ionic/angular';
import { PopoverComponent } from './popover/popover.component';

@NgModule({
  declarations: [ModalBaseComponent, PopoverComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [ModalBaseComponent, PopoverComponent],
})
export class SharedComponentsModule { }
