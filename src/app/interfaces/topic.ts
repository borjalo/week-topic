export interface Topic {
  id: string;
  name: string;
  upvotes: string[];
  downvotes: string[];
  actual: boolean;
}
