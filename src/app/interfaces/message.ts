import firebase from 'firebase/compat';
import Timestamp = firebase.firestore.Timestamp;

export interface Message {
  id: string;
  text: string;
  from: string;
  createdAt: Timestamp;
  likes: string[];
}
