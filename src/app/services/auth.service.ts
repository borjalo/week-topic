import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import User = firebase.User;
import { AngularFireAuth } from '@angular/fire/compat/auth';
import firebase from 'firebase/compat/app';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public firebaseUser$: Observable<User> = this.afAuth.user;

  constructor(public afAuth: AngularFireAuth) {
  }

  async emailLogin(credentials) {
    await this.afAuth.signInWithEmailAndPassword(credentials.email, credentials.password);
  }

  async emailSignUp(credentials) {
    await this.afAuth.createUserWithEmailAndPassword(credentials.email, credentials.password);

    const user = await this.getUser();

    await user.updateProfile({
      displayName: credentials.userName,
    });
  }

  getUser() {
    return this.afAuth.currentUser;
  }

  async signOut() {
    await this.afAuth.signOut();
  }

  handleAuthError(error) {
    let errorMessage = '';

    if (error.code === 'auth/email-already-in-use') {
      errorMessage = 'Ya existe un usuario con este email.';
    }

    return errorMessage;
  }
}
