import { Injectable } from '@angular/core';
import {
  addDoc,
  arrayRemove,
  arrayUnion,
  collection,
  doc,
  Firestore,
  getDocs,
  onSnapshot,
  orderBy,
  query,
  serverTimestamp,
  updateDoc,
  where
} from '@angular/fire/firestore';
import { Topic } from '../interfaces/topic';
import { findIndex } from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {
  constructor(
    private firestore: Firestore,
  ) {
  }

  public getMessages() {
    const q = query(collection(this.firestore, 'messages'), orderBy('createdAt'));
    const messages = [];

    onSnapshot(q, (querySnapshot) => {
      querySnapshot.docChanges().forEach(change => {
        if (change.type === 'added') {
          const message = change.doc.data();
          message.id = change.doc.id;
          messages.push(message);
        }
        if (change.type === 'modified') {
          const messageToModify = findIndex(messages, { id: change.doc.id });
          messages[messageToModify] = change.doc.data();
          messages[messageToModify].id = change.doc.id;
        }
        if (change.type === 'removed') {
          const index = findIndex(messages, { id: change.doc.id });
          if (index > -1) {
            messages.splice(index, 1);
          }
        }
      });
    });

    return messages;
  }

  public getActualTopic() {
    const topicsRef = collection(this.firestore, 'topics');

    return query(topicsRef, where('actual', '==', true));
  }

  public addChatMessage(message: string, userName: string, userId: string, topicId: string) {
    const messagesRef = collection(this.firestore, 'messages');

    return addDoc(messagesRef, {
        user: userId,
        text: message,
        from: userName,
        createdAt: serverTimestamp(),
        likes: [],
        topic: topicId
    });
  }

  async likeMessage(id: string, uid: string) {
    const messageDocRef = doc(this.firestore, `messages`, id);

    await updateDoc(messageDocRef, {
        likes: arrayUnion(uid)
    });
  }

  async unlikeMessage(messageId: string, uid: string) {
    const messageDocRef = doc(this.firestore, `messages`, messageId);

    await updateDoc(messageDocRef, {
      likes: arrayRemove(uid)
    });
  }

  async getHistoryTopics(): Promise<Topic[]> {
    const topicsRef = collection(this.firestore, 'topics');
    const q = query(topicsRef, where('actual', '==', false));
    const querySnapshot = await getDocs(q);
    const topics = [];

    querySnapshot.forEach((document) => {
      topics.push(document.data());
    });

    return topics;
  }

  async upvoteTopic(topicId: string, uid: string) {
    const topicDocRef = doc(this.firestore, 'topics', topicId);

    await updateDoc(topicDocRef, {
      upvotes: arrayUnion(uid)
    });

    await updateDoc(topicDocRef, {
      downvotes: arrayRemove(uid)
    });
  }

  async downvoteTopic(topicId: string, uid: string) {
    const topicDocRef = doc(this.firestore, 'topics', topicId);

    await updateDoc(topicDocRef, {
      downvotes: arrayUnion(uid)
    });

    await updateDoc(topicDocRef, {
      upvotes: arrayRemove(uid)
    });
  }

  async removeUpvoteFromTopic(topicId: string, uid: string) {
    const topicDocRef = doc(this.firestore, 'topics', topicId);

    await updateDoc(topicDocRef, {
      upvotes: arrayRemove(uid)
    });
  }

  async removeDownvoteFromTopic(topicId: string, uid: string) {
    const topicDocRef = doc(this.firestore, 'topics', topicId);

    await updateDoc(topicDocRef, {
      downvotes: arrayRemove(uid)
    });
  }
}
