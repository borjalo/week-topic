import { Injectable } from '@angular/core';
import { filter, first, map } from 'rxjs/operators';
import { AngularFireRemoteConfig } from '@angular/fire/compat/remote-config';

@Injectable({
  providedIn: 'root'
})
export class RemoteConfigService {

  constructor(private remoteConfig: AngularFireRemoteConfig) {
  }

  getValue() {
    return this.remoteConfig.changes.pipe(
      filter(param => param.key === 'week_topic'),
      map(param => param.asString()),
      first()
    );
  }
}
